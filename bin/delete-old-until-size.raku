#!/usr/bin/env raku

use Time::Duration::Parser; # used for duration-to-seconds()
use File::Directory::Tree;  # used for rmtree()

our %multipliers = [K => 1024, M => 1024*1024, G => 1024*1024*1024];

#| Given at least a duration (--older-than) and a folder (defaults to ./), and
#| optionally a size (--free-up), the script will determine a list of files to
#| delete, all older than older-than, the size of which total at most free-up.
#| Alternatively a --prune-down-to option can replace --free-up and will be the
#| target size to obtain after deletion of old files.
#| Priority is given to older files in all cases.
unit sub MAIN (
  #| Folder to operate on
  Str $folder = "./",
  #| Last modified time under which not to consider deleting files, ex: "10 days"
  Str :$older-than!,
  #| Size (in bytes, or with suffix K, M or G) to free up.
  #| If set to 0, will ignore size requirement, and only follow --older-than
  Str :$free-up = "0",
  #| Size (in bytes, or with suffix K, M or G) to strip folder down to
  #| [default: none, refer to free-up]
  Str :$prune-down-to?,
  #| Delete older files in priority, as opposed to largest (which is default)
  Bool :$oldest-first = False,
  #| Simulate deletion
  Bool :$dry-run = True,
  #| Don't prompt before deleting (BEWARE!)
  Bool :$no-prompt = False,
);

# Check options
if ( ! $folder.IO.e ) {
  say "❌ Folder $folder does not exist.";
  exit 2;
}
if (($free-up ne 0) && $prune-down-to) {
  die "❌ Can't provide both non-0 --free-up option, and --prune-down-to option.";
}
my $parsed_older_than = duration-to-seconds($older-than);
if (! $parsed_older_than.defined) {
  die "❌ Failed to parse \"$older-than\" to a duration using Time::Duration::Parser, " ~
    "see https://github.com/perlpilot/p6-Time-Duration-Parser";
}

my Int $used-space = disk_usage($folder.IO);
my Int $parsed_free_up = parse_disk_usage($free-up);
if ($parsed_free_up > 0) {
  # Check if its size is larger than threshold
  if ( $used-space < $parsed_free_up ) {
    say "🎉 Folder $folder is already using less than what we were asked to free up "~
      human($parsed_free_up) ~ ". Nothing to do";
    exit 0;
  }
  say "👷‍♂️ Folder is using more than "~human($parsed_free_up);
} elsif ($prune-down-to) {
  $parsed_free_up = $used-space - parse_disk_usage($prune-down-to);
  say "💻 Currently using "~human($used-space)~", "~human($parsed_free_up)~
    " need to be freed to prune down to "~human(parse_disk_usage($prune-down-to));
} else {
  # $parsed_free_up = 0, and no --prune-down
  say "⚠ Not enforcing any space requirement, --free-up=0";
  $parsed_free_up = 0;
}

say "🕛 Looking for candidates to prune, older than $older-than ($parsed_older_than seconds)";
my %prunable_by_size;
my %prunable_by_age;
my @ages;
my @sizes;
my Instant $start_time=DateTime.now().Instant ;
for $folder.IO.dir -> $file_or_folder {
  next if (! $file_or_folder.e);
  try {
    if ($file_or_folder.l) {
      next; # ignore symlinks
    }
    CATCH {
      when X::IO::DoesNotExist {
        # This bunch is needed because the .l method:
        # "Returns True if the invocant is a path that exists and is a symlink. The
        # method will fail with X::IO::DoesNotExist if the path points to a
        # non-existent filesystem entity."
        next;
      }
    }
  }
  my Instant $changed = $file_or_folder.changed;
  if ($changed < ($start_time - $parsed_older_than) ) {
    my Int $size = disk_usage($file_or_folder).Int;
    my Rat $age = $changed.to-posix[0].Rat;
    %prunable_by_size{$size} = {
      file => $file_or_folder,
      age => $age,
    };
    @sizes.push: $size;
    %prunable_by_age{$age} = {
      file => $file_or_folder,
      size => $size,
    };
    @ages.push: $age;
  }
}

if (@ages.elems == 0) {
  say "😥 All files were modified less than $older-than ($parsed_older_than seconds) ago";
  exit 5;
}
say "🎉 Found "~@ages.elems~" file(s) that are older than $older-than.";
# Look at prunable files, largest/oldest first, and add up how much space can be
# freed by deleting them
my Int $to_delete_total_size = 0;
my Bool $have_enough = False;
say "❓ Offering to delete: ";
my @files_to_delete;
my @sorted_keys;
my %prunable_hash;
if ($oldest-first) {
  say "Time deleted      | Size      |  File name";
  # Here we don't reverse since we waqnt oldest, not newest
  FILE_BY_AGE:
  for @ages.sort -> $age {
    my $this_file = %prunable_by_age{$age}<file>;
    my Int $size = %prunable_by_age{$age}<size>;
    $to_delete_total_size += $size;
    @files_to_delete.push: $this_file;
    my Str $human_size = human($size);
    my Int $sep1 = $human_size.chars;
    my Str $legible_age = DateTime.new(
      formatter => {
        sprintf "%04d-%02d-%02d %02d:%02d",
        .year, .month, .day, .hour, .minute
      },
      Instant.from-posix($age)
    ).Str;
    say  $legible_age,  "  | " , $human_size, " "x(10-$sep1), "| ", $this_file.Str;
    if ( ($parsed_free_up > 0) && ($to_delete_total_size > $parsed_free_up) ) {
      $have_enough = True;
      last FILE_BY_AGE;
    }
  }
} else {
  say "Size      | Time deleted     |  File name";
  # Reverse so that LARGEST are first
  FILE_BY_SIZE:
  for @sizes.sort.reverse -> $size {
    my $this_file = %prunable_by_size{$size}<file>;
    my Rat $age = %prunable_by_size{$size}<age>;
    $to_delete_total_size += $size;
    @files_to_delete.push: $this_file;
    my Str $human_size = human($size);
    my Int $sep1 = $human_size.chars;
    my Str $legible_age = DateTime.new(
      formatter => { 
        sprintf "%04d-%02d-%02d %02d:%02d",
        .year, .month, .day, .hour, .minute
      },
      Instant.from-posix($age)
    ).Str;
    say $human_size, " "x(10-$sep1),  "| ", $legible_age,  " | " , $this_file.Str;
    if ( ($parsed_free_up > 0) && ($to_delete_total_size > $parsed_free_up) ) {
      $have_enough = True;
      last FILE_BY_SIZE;
    }
  }
}

if ($have_enough) {
  if ($oldest-first) {
    say "💪 If we delete the "~@files_to_delete.elems~" oldest - older than "~
      "$older-than files, that'll free "~human($to_delete_total_size)~
      ", which is more than the "~human($parsed_free_up)~" objective.";
  } else {
    say "💪 If we delete the "~@files_to_delete.elems~" largest - older than "~
      "$older-than files, that'll free "~human($to_delete_total_size)~
      ", which is more than the "~human($parsed_free_up)~" objective.";
  }
} else {
  if ($parsed_free_up == 0) {
    say "💪 There are "~@files_to_delete.elems~" older than "~
      "$older-than files, occupying  "~human($to_delete_total_size);
  } else {
    say "😢 Unable to reduce folder usage by "~human($parsed_free_up)~
      " even by deleting all files older than $older-than, which total only "~
      human($to_delete_total_size);
  }
}

if ($no-prompt) {
  say "☢️ The --no-prompt option is selected. Proceeding.";
} else {
  say '🌴 DRY RUN: whatever you answer, we ' ~
    'won\'t perform any deletions (use --/dry-run to delete)' if $dry-run; 
  PROMPT:
  while True {
    my $answer = prompt("❓ Proceed? y/N: ");
    given $answer {
      when "n"|"N"|"" {
        say "🏃 Exiting without doing anything.";
        exit 0;
      };
      when "y"|"Y" {
        say "➡ Proceeding.";
        last PROMPT;
      };
      default {
        say "⚠ Unknown answer: $answer, try again please.";
        next PROMPT;
      };
    }
  }
}

my Int $success = 0;
my Int $failed = 0;
my Str @failed_files;
say '🌴 DRY RUN: not actually performing deletions (use --/dry-run to delete)' if $dry-run; 
for @files_to_delete.map({.IO}) -> $file_to_delete {
  print "Deleting \"$file_to_delete\": ";
  my Bool $rm_result = True;
  if (!$dry-run) {
    if ($file_to_delete.f) {
      $rm_result = $file_to_delete.unlink;
    }
    if ($file_to_delete.d) {
      $rm_result = rmtree $file_to_delete;
    }
  }
  if ($rm_result) {
    say "OK ✅";
    $success++;
  } else {
    say "FAILED ❌";
    @failed_files.push: $file_to_delete.Str;
    $failed++;
  }
};
say "🎉 Deletion complete, had $success / " ~ @files_to_delete.elems ~
  " succeeded and $failed / " ~ @files_to_delete.elems ~ " failed.";
if ($failed > 0) {
  say "⚠ Failed for files: ";
  say "  ", @failed_files.join(", ");
}
say '🌴 DRY RUN: whatever it says above'~
  ', we didn\'t perform deletions (use --/dry-run to delete)' if $dry-run; 

exit 0;


#############
# FUNCTIONS #
#############

#| IO.s on a folder is, per the docs "up to the operating system", and has
#| a somewhat ambiguous side to it: actual size the folder uses? Recursive?
#| So here we implement what we actually want it to be, the sum of IO.s on all
#| true files included.
sub disk_usage(
  IO::Path $this,
  --> Int
) {
  if ($this.f) {
    return $this.s;
  }
  my Int $size = 0;
  # Recursive loop
  my @todo = $this;
  while @todo {
    for @todo.pop.dir -> $path {
      $size += $path.s   if $path.f;
      @todo.push: $path  if $path.d;
    }
  }
  return $size;
}

sub parse_disk_usage(
  Str $string,
  --> Int
) {
  grammar File::Size {
                  # 1 Mb or 1KB or 10GB or 100 G…
      token TOP { <digits>\s?<letter>?(b|B)? }; # this is implicitly anchored start/end
      token digits { \d+ };
      token letter { (k|K|m|M|g|G) }
  }
  my File::Size $parsed_size = File::Size.parse($string) ||
    die "❌ Failed to parse $string into a size in bytes.";
  my Int $int = $parsed_size<digits>.Str.Int;
  if ($parsed_size<letter>) {
     $int = $int * %multipliers{ $parsed_size<letter>.Str.uc };
  }
  return $int;
}

sub human (
  Int $size,
  --> Str
) {
  for <G M K> -> $mult { # Largest to smallest
    next if (%multipliers{$mult} > $size);
    if ($size >= %multipliers{$mult}) {
      return sprintf '%.1f%sb', ($size / %multipliers{$mult}), $mult;
    }
  }
  return $size.Str ~ "b";
}
