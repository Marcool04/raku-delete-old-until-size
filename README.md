# Synopsis

```
$ delete-old-until-size.raku --older-than "30 days"
Not enforcing any space requirement, --free-up=0
Looking for candidates to prune, older than 30 days (2592000 seconds)
Found 4 file(s) that are older than 30 days.
Offering to delete:
880       | ./file1
8.4Kb     | ./file2
5.0Kb     | ./file3
320       | ./folder1
There are 4 older than 30 days – files, occupying  14.5Kb
❓ Proceed? Y/n:^C

$ delete-old-until-size.raku --free-up=10K --older-than="1 second"
Folder is using more than 10.0Kb
Looking for candidates to prune, older than 1 second (1 seconds)
Found 9 file(s) that are older than 1 second.
Offering to delete:
880       | ./file1
8.4Kb     | ./file2
786       | ./file4
If we delete the 3 largest – older than 1 second – files, that'll free 10.0Kb, which is more than the 10.0Kb objective.
❓ Proceed? Y/n:^C
```


# Description

Given at least a duration (`--older-than`) and a folder (by default, present working directory `./`), and optionally a size (`--free-up`), the script will determine a list of files to delete, all older than older-than, the size of which total at most free-up. Alternatively a prune-down-to option can replace free-up and will be the target size to obtain after deletion of old files. Priority is given to older files in all cases.

Note that the script works on all files and folders contained in the folder passed as positional argument, and considers the effective disk-usage of folders as their "size". So if you have a folder occupying 28Mb of disk space, that will effectively be treated as a 28Mb file, for the purpose of sorting / determining what to delete.


# Use case

I personally use this on my "Trash" folder regularly, so that I can free up space while conserving traces of (small) old files I might conceivably still need. Other uses could be thought of no doubt.


# AUTHOR
Mark Collins https://gitlab.com/Marcool04


# COPYRIGHT AND LICENCE
Copyright 2020 Mark Collins

This work is free. You can redistribute it and/or modify it under the terms of the Do What The Fuck You Want To Public License, Version 2, as published by Sam Hocevar. See the LICENCE file for (no) more details.
