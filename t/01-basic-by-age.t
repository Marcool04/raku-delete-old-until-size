#!usr/bin/env raku

use Test;
use File::Directory::Tree;

signal(SIGINT).tap( { say "Interrupted"; exit } );

my $test_folder = "test_folder".IO;
say "Creating folder $test_folder";
$test_folder.mkdir;
ok $test_folder.IO.d, "$test_folder exists and is a directory pre-test";
my @files;
say "Will sleep 1 second between files, to make sure their creation time is spread out";
print  "Creating test files: ";
for (0..10) -> $file_number {
  my $test_file = "$test_folder/file_$file_number".IO;
  # Increasingly more than 10 Mb files
  $test_file.spurt( "/dev/random".IO.open.read( (10+$file_number)*1024*1024 ) );
  #ok $test_file.IO.e, "test file $test_file exists pre-test";
  #is $test_file.IO.s, (10+$file_number)*1024*1024, "test file $test_file occupied correct disk space";
  @files.push: $test_file.absolute;
  print "$file_number ";
  sleep 1;
}

print "\nDetermining current test folder disk usage: ";
my Int $pre-test-usage = disk_usage($test_folder);
say $pre-test-usage;

say "Running delete-old-until-size.raku with --oldest-first option";
run(
  "./bin/delete-old-until-size.raku",
  "--free-up=20M",
  "--older-than=1 second",
  "--oldest-first",
  "--no-prompt",
  "--/dry-run",
  $test_folder,
  :out
).out.lines.map( -> $line { say "FROM PROC: $line" });

ok disk_usage($test_folder) < ($pre-test-usage - 20*1024*1024), "Folder size reduced ok";
ok ! "$test_folder/file_0".IO.e, "File 0 was deleted (oldest).";
ok ! "$test_folder/file_1".IO.e, "File 1 was deleted (second oldest).";
ok "$test_folder/file_2".IO.e, "File 2 was NOT deleted (third oldest).";

done-testing;
exit;

END {
  print "Deleting $test_folder: ";
  my $rm_result = rmtree $test_folder;
  say ($rm_result ?? "OK" !! "FAILED");
}

#| IO.s on a folder is, per the docs "up to the operating system", and has
#| a somewhat ambiguous side to it: actual size the folder uses? Recursive?
#| So here we implement what we actually want it to be, the sum of IO.s on all
#| true files included.
sub disk_usage(
  #| A folder to determin disk usage for (REQUIRED)
  IO::Path $this,
  #| Returns size in bytes
  --> Int
) {
  if ($this.f) {
    return $this.s;
  }
  my Int $size = 0;
  # Recursive loop
  my @todo = $this;
  while @todo {
    for @todo.pop.dir -> $path {
      $size += $path.s   if $path.f;
      @todo.push: $path  if $path.d;
    }
  }
  return $size;
}
