#!usr/bin/env raku

use Test;
use File::Directory::Tree;

signal(SIGINT).tap( { say "Interrupted"; exit } );

my $test_folder = "test_folder".IO;
say "Creating folder $test_folder";
$test_folder.mkdir;
#ok $test_folder.IO.d, "$test_folder exists and is a directory pre-test";
my @files;
print "Creating test files: ";
for (0..10) -> $file_number {
  my $test_file = "$test_folder/file_$file_number".IO;
  # Increasingly more than 10 Mb files
  $test_file.spurt( "/dev/random".IO.open.read( (10+$file_number)*1024*1024 ) );
  # ok $test_file.IO.e, "test file $test_file exists pre-test";
  # is $test_file.IO.s, (10+$file_number)*1024*1024, "test file $test_file occupied correct disk space";
  @files.push: $test_file.absolute;
  print "$file_number ";
}
say "\nSleeping 2 seconds, to make sure all files are older than 1 second.";
sleep 2;

print "Determining current test folder disk usage: ";
my Int $pre-test-usage = disk_usage($test_folder);
say $pre-test-usage;

say "Running delete-old-until-size.raku";
run(
  "./bin/delete-old-until-size.raku",
  "--free-up=20M",
  "--older-than=1 second",
  "--no-prompt",
  "--/dry-run",
  $test_folder,
  :out
).out.lines.map( -> $line { say "FROM PROC: $line" });

ok disk_usage($test_folder) < ($pre-test-usage - 20*1024*1024), "Folder size reduced ok";
ok ! "$test_folder/file_10".IO.e, "File 10 was deleted (largest).";
ok ! "$test_folder/file_9".IO.e, "File 9 was deleted (second largest).";
ok "$test_folder/file_8".IO.e, "File 8 was NOT deleted (third largest).";

done-testing;
exit;

END {
  print "Deleting $test_folder: ";
  my $rm_result = rmtree $test_folder;
  say ($rm_result ?? "OK" !! "FAILED");
}

#| IO.s on a folder is, per the docs "up to the operating system", and has
#| a somewhat ambiguous side to it: actual size the folder uses? Recursive?
#| So here we implement what we actually want it to be, the sum of IO.s on all
#| true files included.
sub disk_usage(
  #| A folder to determin disk usage for (REQUIRED)
  IO::Path $this,
  #| Returns size in bytes
  --> Int
) {
  if ($this.f) {
    return $this.s;
  }
  my Int $size = 0;
  # Recursive loop
  my @todo = $this;
  while @todo {
    for @todo.pop.dir -> $path {
      $size += $path.s   if $path.f;
      @todo.push: $path  if $path.d;
    }
  }
  return $size;
}
